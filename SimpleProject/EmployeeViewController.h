//
//  EmployeeViewController.h
//  SimpleProject
//
//  Created by user on 08/04/19.
//  Copyright © 2019 sumedcoding. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface EmployeeViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@end

NS_ASSUME_NONNULL_END
