//
//  GlobalVariable.h
//  SimpleProject
//
//  Created by user on 07/04/19.
//  Copyright © 2019 sumedcoding. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UserModel.h"

NS_ASSUME_NONNULL_BEGIN
extern NSString *globalFullname;

@interface GlobalVariable : NSObject

@property(nonatomic, retain) NSString *fullname;
@property(nonatomic, retain) UserModel *userModel;

//TODO: Create base url for API

+(GlobalVariable *)sharedSingleton;

@end

NS_ASSUME_NONNULL_END
