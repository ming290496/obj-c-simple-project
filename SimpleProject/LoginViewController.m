//
//  LoginViewController.m
//  SimpleProject
//
//  Created by user on 07/04/19.
//  Copyright © 2019 sumedcoding. All rights reserved.
//

#import "LoginViewController.h"
#import "AFNetworking.h"
#import "GlobalVariable.h"
#import "UserModel.h"

@interface LoginViewController ()
@property (weak, nonatomic) IBOutlet UITextField *tf_username;
@property (weak, nonatomic) IBOutlet UITextField *tf_password;

@end

@implementation LoginViewController

GlobalVariable *gvLogin;
UIAlertController *progressDialog;

- (void)viewDidLoad {
    [super viewDidLoad];
    gvLogin = [GlobalVariable sharedSingleton];
    
    // Do any additional setup after loading the view.
}
- (IBAction)btn_login_onclick:(id)sender {
    if([_tf_username.text isEqualToString:@""] || [_tf_password.text isEqualToString:@""]){
        [self showAlert:@"Login Failed" :@"Username and password must be field"];
    }else{
        
        progressDialog = [UIAlertController alertControllerWithTitle:@"Loading" message:@"Please wait..." preferredStyle:UIAlertControllerStyleAlert];
        [self presentViewController:progressDialog animated:YES completion:^{
            
        }];
        
        NSURL *URL = [NSURL URLWithString:@"http://10.98.0.154/firstproject/api/controller/auth.php?method=login"];
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/html", nil];
        
        NSDictionary *params = [[NSDictionary alloc] initWithObjectsAndKeys:
            _tf_username.text, @"username",
            _tf_password.text, @"password",
            nil];
        
        [manager POST:URL.absoluteString parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            //[self dismissProgressDialog];
            //UNTUK CHECK RESPONSE OBJECT VALUE NULL ATAU TIDAK
            if([responseObject[@"error"] isKindOfClass:[NSNull class]]){
                NSDictionary *response = (NSDictionary *) responseObject[@"result"];
                UserModel *userModel = [[UserModel alloc] initWithDictionary:response];
                gvLogin.userModel = userModel;
                NSLog(@"%@", gvLogin.userModel.fullname);
                [self performSegueWithIdentifier:@"segueLoginToHome" sender:self];
            }else{
                [self dismissProgressDialog:^{
                    [self showAlert:@"Login Failed" :responseObject[@"error"]];
                }];
            }
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            NSLog(@"FAILURE : %@", error);
            [self dismissProgressDialog:^{
                [self showAlert:@"Login Failed" :[error localizedDescription]];
            }];
        }];
    }
}

- (void) showAlert:(NSString *) title :(NSString *) message{
    UIAlertController *alert = [UIAlertController
                                alertControllerWithTitle:title
                                message:message
                                preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okButton = [UIAlertAction
                               actionWithTitle:@"Ok"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action) {
                                   //handler action
                               }];
    
    [alert addAction:okButton];
    
    [self presentViewController:alert animated:YES completion:nil];
}

- (void) showProgressDialog: (NSString *)title : (NSString *) message{
    progressDialog = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    [self presentViewController:progressDialog animated:NO completion:nil];
    
//    UIActivityIndicatorView *indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    
    // Adjust the indicator so it is up a few pixels from the bottom of the alert
    //[indicator startAnimating];
}

//THIS IS A BLOCK AS A PARAM
- (void) dismissProgressDialog: (void (^)(void))completion{
    [progressDialog dismissViewControllerAnimated:NO completion:^{
        completion();
    }];
}

@end
