//
//  UserModel.h
//  SimpleProject
//
//  Created by user on 07/04/19.
//  Copyright © 2019 sumedcoding. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface UserModel : NSObject{
    NSNumber *id;
    NSString *username;
    NSString *fullname;
    NSString *address;
    NSString *email;
}

@property (nonatomic, strong) NSNumber *id;
@property (nonatomic, strong) NSString *username;
@property (nonatomic, strong) NSString *fullname;
@property (nonatomic, strong) NSString *address;
@property (nonatomic, strong) NSString *email;

-(id)initWithDictionary:(NSDictionary *)sourceDirectory;

@end

NS_ASSUME_NONNULL_END
