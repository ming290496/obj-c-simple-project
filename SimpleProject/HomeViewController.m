//
//  HomeViewController.m
//  SimpleProject
//
//  Created by user on 07/04/19.
//  Copyright © 2019 sumedcoding. All rights reserved.
//

#import "HomeViewController.h"
#import "GlobalVariable.h"

@interface HomeViewController ()

@property (weak, nonatomic) IBOutlet UILabel *lbl_welcome;

@end

@implementation HomeViewController

GlobalVariable *gvHome;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    gvHome = [GlobalVariable sharedSingleton];
    UserModel *userModel = gvHome.userModel;
    
    _lbl_welcome.text = [NSString stringWithFormat:@"Welcome, %@", userModel.fullname];
}
- (IBAction)btn_logout_onclick:(id)sender {
    UIAlertController *alert = [UIAlertController
                                alertControllerWithTitle:@"Logout"
                                message:@"Are you sure?"
                                preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *yesButton = [UIAlertAction
                                actionWithTitle:@"Yes"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action) {
                                   [self performSegueWithIdentifier:@"segueHomeToLogin" sender:self];
                                }];
    
    UIAlertAction *noButton = [UIAlertAction
                                actionWithTitle:@"No"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action) {

                                }];
    
    [alert addAction:noButton];
    [alert addAction:yesButton];
    
    [self presentViewController:alert animated:YES completion:nil];
}
@end
