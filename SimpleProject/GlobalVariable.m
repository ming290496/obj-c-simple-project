//
//  GlobalVariable.m
//  SimpleProject
//
//  Created by user on 07/04/19.
//  Copyright © 2019 sumedcoding. All rights reserved.
//

#import "GlobalVariable.h"

NSString *globalFullname = @"HELLO WORLD";

@implementation GlobalVariable

@synthesize fullname;

static GlobalVariable *shared = NULL;

-(id)init{
    if(self = [super init]){
        //fullname = @"HENLO";
    }
    return self;
}

+ (GlobalVariable *) sharedSingleton{
    @synchronized (shared) {
        if(!shared || shared == NULL){
            shared = [[GlobalVariable alloc] init];
        }
        
        return shared;
    }
}
@end
