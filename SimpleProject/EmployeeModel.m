//
//  EmployeeModel.m
//  SimpleProject
//
//  Created by user on 08/04/19.
//  Copyright © 2019 sumedcoding. All rights reserved.
//

#import "EmployeeModel.h"

@implementation EmployeeModel

@synthesize id, fullname, gender, title, language, job_title, university;

- (id) initWithDictionary:(NSDictionary *)sourceDirectory{
    self = [super init];
    if(self != nil){
        self.id = [sourceDirectory valueForKey:@"id"];
        self.fullname = [sourceDirectory valueForKey:@"fullname"];
        self.gender = [sourceDirectory valueForKey:@"gender"];
        self.title = [sourceDirectory valueForKey:@"title"];
        self.language = [sourceDirectory valueForKey:@"language"];
        self.job_title = [sourceDirectory valueForKey:@"job_title"];
        self.university = [sourceDirectory valueForKey:@"university"];
    }
    return self;
}

@end
