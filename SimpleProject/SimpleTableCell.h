//
//  SimpleTableCell.h
//  SimpleProject
//
//  Created by user on 10/04/19.
//  Copyright © 2019 sumedcoding. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SimpleTableCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *genderLabel;
@property (weak, nonatomic) IBOutlet UILabel *fullnameLabel;
@end

NS_ASSUME_NONNULL_END
