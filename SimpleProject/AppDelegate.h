//
//  AppDelegate.h
//  SimpleProject
//
//  Created by user on 07/04/19.
//  Copyright © 2019 sumedcoding. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong) NSPersistentContainer *persistentContainer;

- (void)saveContext;


@end

