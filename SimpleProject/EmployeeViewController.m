//
//  EmployeeViewController.m
//  SimpleProject
//
//  Created by user on 08/04/19.
//  Copyright © 2019 sumedcoding. All rights reserved.
//

#import "EmployeeViewController.h"
#import "AFNetworking.h"
#import "SimpleTableCell.h"
#import "EmployeeModel.h"

@interface EmployeeViewController ()
@property (weak, nonatomic) IBOutlet UITableView *tbl_view;
@end

@implementation EmployeeViewController

NSMutableArray *dataFullname;
NSMutableArray *dataGender;
NSMutableArray *dataEmployee;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    dataFullname = [NSMutableArray new];
    dataGender = [NSMutableArray new];
    dataEmployee = [NSMutableArray new];

    [self getDataEmployee];
    //_tbl_view.rowHeight = 70;
}

- (void) getDataEmployee{
    NSURL *URL = [NSURL URLWithString:@"http://10.98.0.154/firstproject/api/controller/employee.php?method=select_ms_employee"];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/html", nil];
    
    [manager POST:URL.absoluteString parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        //UNTUK CHECK RESPONSE OBJECT VALUE NULL ATAU TIDAK
        if([responseObject[@"error"] isKindOfClass:[NSNull class]]){
            NSDictionary *json = (NSDictionary *) responseObject;
            NSArray *array = [json objectForKey:@"result"];
            
            for(NSDictionary *row in array){
                EmployeeModel *model = [[EmployeeModel alloc] initWithDictionary:row];
                [dataEmployee addObject:model];
                
                [dataFullname addObject: [row objectForKey:@"fullname"]];
                [dataGender addObject: [row objectForKey:@"gender"]];
                [self->_tbl_view reloadData];
            }
        }else{
            NSLog(@"FAILURE");
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"FAILURE : %@", error);
    }];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [dataFullname count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *simpleTableIdentifier = @"SimpleTableItem";
    
    SimpleTableCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if(cell == nil){
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"SimpleTableCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
        
//        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    EmployeeModel *model = [dataEmployee objectAtIndex:indexPath.row];
    
    cell.fullnameLabel.text = model.fullname;
    cell.genderLabel.text = model.gender;
    
    //cell.genderLabel.text = [dataGender objectAtIndex:indexPath.row];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 78;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSLog(@"%@", [dataFullname objectAtIndex:indexPath.row]);
}

@end
