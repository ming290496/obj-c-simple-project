//
//  SimpleTableCell.m
//  SimpleProject
//
//  Created by user on 10/04/19.
//  Copyright © 2019 sumedcoding. All rights reserved.
//

#import "SimpleTableCell.h"

@implementation SimpleTableCell

@synthesize fullnameLabel = _fullnameLabel;
@synthesize genderLabel = _genderLabel;

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
