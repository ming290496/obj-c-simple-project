//
//  UserModel.m
//  SimpleProject
//
//  Created by user on 07/04/19.
//  Copyright © 2019 sumedcoding. All rights reserved.
//

#import "UserModel.h"

@implementation UserModel

@synthesize id, username, fullname, address, email;

- (id) initWithDictionary:(NSDictionary *)sourceDirectory{
    self = [super init];
    if(self != nil){
        self.id = [sourceDirectory valueForKey:@"id"];
        self.username = [sourceDirectory valueForKey:@"username"];
        self.fullname = [sourceDirectory valueForKey:@"fullname"];
        self.address = [sourceDirectory valueForKey:@"address"];
        self.email = [sourceDirectory valueForKey:@"email"];
    }
    return self;
}

@end
