//
//  EmployeeModel.h
//  SimpleProject
//
//  Created by user on 08/04/19.
//  Copyright © 2019 sumedcoding. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface EmployeeModel : NSObject{
    NSNumber *id;
    NSString *fullname;
    NSString *gender;
    NSString *title;
    NSString *language;
    NSString *job_title;
    NSString *university;
}

@property (nonatomic, strong) NSNumber *id;
@property (nonatomic, strong) NSString *fullname;
@property (nonatomic, strong) NSString *gender;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *language;
@property (nonatomic, strong) NSString *job_title;
@property (nonatomic, strong) NSString *university;


-(id)initWithDictionary:(NSDictionary *)sourceDirectory;

@end

NS_ASSUME_NONNULL_END
